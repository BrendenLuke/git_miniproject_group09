﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchweapon : MonoBehaviour {
	public GameObject wGun;
	public GameObject aGun;
	public GameObject dGun;
	public GameObject rGun;

	// Use this for initialization
	void Awake () {
		aGun.SetActive (true);
		wGun.SetActive (false);
		dGun.SetActive (false);
		rGun.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("y")) {
            PlayerScript.playerScript.fruitType = PlayerScript.FruitType.Watermelon;
			aGun.SetActive (false);
			wGun.SetActive (true);
			dGun.SetActive (false);
			rGun.SetActive (false);
		}
		if (Input.GetKeyDown ("u")) {
            PlayerScript.playerScript.fruitType = PlayerScript.FruitType.Durian;
            aGun.SetActive (true);
			wGun.SetActive (false);
			dGun.SetActive (false);
			rGun.SetActive (false);
		}
		if (Input.GetKeyDown ("i")) {
            PlayerScript.playerScript.fruitType = PlayerScript.FruitType.Rashberry;
            aGun.SetActive (false);
			wGun.SetActive (false);
			dGun.SetActive (true);
			rGun.SetActive (false);
		}
		if (Input.GetKeyDown ("o")) {
            PlayerScript.playerScript.fruitType = PlayerScript.FruitType.Apricot;
            aGun.SetActive (false);
			wGun.SetActive (false);
			dGun.SetActive (false);
			rGun.SetActive (true);
		}
	}
}
