﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyer : MonoBehaviour {

    public float health = 3f;

    public List<GameObject> healthObj;
    public UnityEngine.UI.Text gameOverText;

    void Start() {
        gameOverText.gameObject.SetActive(false);
    }

    void Update() {
        if (health > -1 && health < 1) {
            health = -2;

            StartCoroutine(MoveToEnd());
        }
    }

    IEnumerator MoveToEnd() {
        gameOverText.gameObject.SetActive(true);
        gameOverText.text = "You Lose\nYour Score is : " + Score.score;
        yield return new WaitForSeconds(3);
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    private void OnTriggerEnter2D(Collider2D col) {
        if (col.tag == "fruit") {
            if (health >= 0) {
                healthObj[(int)health - 1].SetActive(false);
            }

            health -= 1;
           // print("GG");
            Destroy(col.gameObject);
        }
    }
}