﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public float spawnRate = 2f;
    [SerializeField]
    private GameObject[] fruit;
    private BoxCollider2D coll;
    float line1, line2;
    //public AudioSource audioSource;
    //public AudioClip audioClip;

    void Awake() {
        coll = GetComponent<BoxCollider2D>();
        line1 = transform.position.x - coll.bounds.size.x / 2f;
        line2 = transform.position.x + coll.bounds.size.x / 2f;
    }

    void Start() {
        //audioSource.clip = audioClip;
        //audioSource.Play();
        InvokeRepeating("SpawnFruit", 0, spawnRate);
    }

    void SpawnFruit() {
        Vector3 pos = transform.position;
        pos.x = Random.Range(line1, line2);

        int spawnNumber = Random.Range(0, fruit.Length);
        GameObject temp = Instantiate(fruit[spawnNumber], pos, Quaternion.identity);

        switch (spawnNumber) {
            case 0:
                temp.name = "Watermelon";
                break;
            case 1:
                temp.name = "Durian";
                break;
            case 2:
                temp.name = "Rasberry";
                break;
            case 3:
                temp.name = "Apricote";
                break;
        }
    }
}