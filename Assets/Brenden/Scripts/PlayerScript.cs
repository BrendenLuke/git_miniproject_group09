﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {
    public static PlayerScript playerScript;
	public AudioClip shootingclip;
	private AudioSource shootingsource;
    public enum FruitType { Apricot, Watermelon, Rashberry, Durian };
    public FruitType fruitType;

    public float moveSpeed = 3f;
    public float bulletSpeed = 5f;
    float velX;
    float velY;
    bool facingRight = true;
    float intervalSpeed = .5f;

    public GameObject bullet;

    Rigidbody2D rigBody;
    Animator animator;
    
    void Start() { 
		shootingsource = GetComponent<AudioSource> ();

        if (playerScript == null) playerScript = this;
        rigBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update() {
        Movement();
        Shoot();
    }

    void Shoot() {
        if (intervalSpeed > 0) {
            intervalSpeed -= Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.Space) && intervalSpeed <= 0) {
            intervalSpeed = .5f;
            animator.SetTrigger("Shoot");
			shootingsource.clip = shootingclip;
			shootingsource.Play ();


            GameObject temp = Instantiate(bullet, transform.position, Quaternion.identity);
            temp.GetComponent<BulletScript>().speed = bulletSpeed;

            switch (fruitType) {
                case FruitType.Apricot:
                    temp.name = "Apricote";
                    break;
                case FruitType.Watermelon:
                    temp.name = "Watermelon";
                    break;
                case FruitType.Rashberry:
                    temp.name = "Rasberry";
                    break;
                case FruitType.Durian:
                    temp.name = "Durian";
                    break;
            }

        }
    }

    void Movement() {
        velX = Input.GetAxisRaw("Horizontal");
        velY = rigBody.velocity.y;
        rigBody.velocity = new Vector2(velX * moveSpeed, velY);

        Vector3 localScale = transform.localScale;
        if (velX > 0) {
            facingRight = true;
        }
        else if (velX < 0) {
            facingRight = false;
        }

        if (((facingRight) && (localScale.x < 0)) || ((!facingRight) && (localScale.x > 0))) {
            localScale.x *= -1;
        }
        transform.localScale = localScale;
    }
}