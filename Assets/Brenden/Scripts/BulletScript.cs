﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;

public class BulletScript : MonoBehaviour {

    [HideInInspector] public float speed = 5f;
    public Score score;
    void Start() {
        score = GameObject.Find("Score").GetComponent<Score>();
    }

    void Update () {
        transform.position += Vector3.up * Time.deltaTime * speed;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.name == "Apricote" && gameObject.name == "Apricote") {
            score.PlusScore();
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        else if (other.gameObject.name == "Watermelon" && gameObject.name == "Watermelon") {
            score.PlusScore();
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        else if (other.gameObject.name == "Rasberry" && gameObject.name == "Rasberry") {
            score.PlusScore();
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        else if (other.gameObject.name == "Durian" && gameObject.name == "Durian") {
            score.PlusScore();
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
