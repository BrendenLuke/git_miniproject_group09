﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	public GameObject menu;
	public GameObject Instruction;

	public void GoToInstruction()
	{
		// Oh you touch my tralala
		// Oh you touch my ding ding dong
		menu.gameObject.SetActive(false);
		Instruction.gameObject.SetActive (true);
	}

	public void Quit()
	{
		Application.Quit ();
		print ("Quit");
	}

	public void GotoMenu()
	{
		menu.gameObject.SetActive(true);
		Instruction.gameObject.SetActive (false);
	}

    public void Play() {
        SceneManager.LoadScene("Git_Miniproject_Group9");
    }
}
