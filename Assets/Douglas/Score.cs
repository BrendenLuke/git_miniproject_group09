﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public static int score;
    public Text scoreText;
    private AudioSource audioSource;
    
	// Use this for initialization
	void Start () {
        score = 0;
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        scoreText.text = "score : " + score.ToString();
        
	}
    public void PlusScore()
    {
        score += 10;
        audioSource.Play();
    }
}
